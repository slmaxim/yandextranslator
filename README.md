## Test Framework for your API

### Presets
* install jdk8
* install lombok plugin
* customize lombok plugin (Build, Execution, Deployment --> Compiler --> Annotation Processors, Enable annotation processing)
* enjoy...

>if you want to run a YandexTranslatorTest, generate and insert the token into YandexTranslatorGateway https://translate.yandex.ru/developers/keys or use mine.