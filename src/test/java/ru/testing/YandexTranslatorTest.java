package ru.testing;

import org.junit.jupiter.api.DisplayName;
import ru.testing.entities.Translator;
import ru.testing.gateway.YandexTranslatorGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class YandexTranslatorTest {
    private static final String POST_TEXT = "Hello World!";
    private static final String EXPECTED_TEXT = "Всем Привет!";


    @Test
    @DisplayName("Переводчик")
    public void sendText(){
        YandexTranslatorGateway yandexTranslatorGateway = new YandexTranslatorGateway();
        Translator translator = yandexTranslatorGateway.getTranslate(POST_TEXT);
        Assertions.assertEquals(EXPECTED_TEXT, String.join("; ", translator.getText()));
    }

}
