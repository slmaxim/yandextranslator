package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.Translator;

@Slf4j
public class YandexTranslatorGateway {
    private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private static final String TOKEN = "trnsl.1.1.20190828T180934Z.6bf8894550d68549.c85bac170499fc4a2c4aec19def177ed5d5637ca";

    @SneakyThrows
    public Translator getTranslate(String postText) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "*/*")
                .field("key", TOKEN)
                .field("text", postText)
                .field("lang", "en-ru")
                .field("format", "plain")
                .field("options", 1)
                .asString();
        String strResponse = response.getBody();
        log.info("response: "+strResponse);
        return gson.fromJson(strResponse, Translator.class);
    }
}